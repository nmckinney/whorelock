#include "activethread.h"

ActiveThread::ActiveThread(QObject *parent, QWidget* widget) :
    QObject(parent)
{
    this->widget = widget;
}

void ActiveThread::threadStart(void)
{
    while (true)
    {
        if(widget)
        {
            if (!widget->isActiveWindow())
            {
                QApplication::setActiveWindow(widget);
            }
        }
    }
}
