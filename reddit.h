#ifndef REDDIT_H
#define REDDIT_H

#include <QObject>
#include <QString>
#include <QPair>
#include <QSettings>
#include <QDebug>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QUrl>
#include <QNetworkReply>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonParseError>
#include <QJsonArray>
#include <QHttpMultiPart>
#include <QHttpPart>
#include <QList>

typedef QPair<QString,QString> RedditCredential;

enum HTTP_REQUEST_TYPE
{
    GET = 1,
    POST,
};

class Reddit : public QObject
{
    Q_OBJECT
private:
    RedditCredential loginUser;
    QNetworkAccessManager* qnam;
    QString subreddit;
    QString code;
    QString modhash;
    QString thingID;

    void getCredentialsFromConfig(void);
    void login(void);
    void sendRequest(QUrl url, HTTP_REQUEST_TYPE type_request = GET, QList<QHttpPart>* list = 0);
    void comment(void);
public:
    explicit Reddit(QObject *parent = 0, QString code = "");
signals:
    void finished(void);
    void error(void);
    void loggedIn(void);
    void gotSubreddit(void);
    void sentCode(void);
public slots:
    void start(void);
    void gotReply(QNetworkReply* reply);
private slots:
    void findSubreddit(void);
    void sendCode(void);
};

#endif // REDDIT_H
