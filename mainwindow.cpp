#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QWebView *view = new QWebView(this);
    view->load(QUrl::fromLocalFile(Install::getInstallPath() + "fed_lock.html"));
    view->page()->setLinkDelegationPolicy(QWebPage::DelegateAllLinks);
    view->page()->action(QWebPage::Reload)->setVisible(false);
    view->show();
    view->setWindowFlags(Qt::FramelessWindowHint);
    QObject::connect(view, SIGNAL(linkClicked(QUrl)), this, SLOT(getCode()));

    this->setWindowFlags(Qt::FramelessWindowHint|Qt::WindowStaysOnTopHint);
    this->setCentralWidget(view);
    this->setContextMenuPolicy(Qt::PreventContextMenu);
}


MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::getCode(void)
{
    QInputDialog* dlg = new QInputDialog(this);
    dlg->setInputMode(QInputDialog::TextInput);
    dlg->setWindowTitle("Enter MoneyPak Code");
    dlg->resize(500, 100);

    QObject::connect(dlg, SIGNAL(textValueSelected(QString)), this, SLOT(getTextValue(QString)));

    dlg->exec();
}

void MainWindow::getTextValue(QString str)
{
    if (!str.isEmpty())
    {
//        qDebug() << "Entered code:" << str;
        redd = new Reddit(0, str);
        redd->start();
        QObject::connect(redd, SIGNAL(sentCode()), redd, SLOT(deleteLater()));
    }
}
