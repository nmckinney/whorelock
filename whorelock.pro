#-------------------------------------------------
#
# Project created by QtCreator 2013-01-24T19:52:24
#
#-------------------------------------------------

QT       += core gui webkitwidgets network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = whorelock
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    defensethread.cpp \
    install.cpp \
    reddit.cpp \
    activethread.cpp

HEADERS  += mainwindow.h \
    defensethread.h \
    install.h \
    reddit.h \
    activethread.h

FORMS    += mainwindow.ui

RESOURCES += \
    resources.qrc

OTHER_FILES += \
    config.ini
