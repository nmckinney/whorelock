#include "defensethread.h"

DefenseThread::DefenseThread(QObject *parent) :
    QObject(parent)
{
}


void DefenseThread::threadStart(void)
{
    QSettings settings(":/files/config.ini", QSettings::IniFormat);
    settings.beginGroup("blacklist");
    list = new QStringList(settings.value(settings.childKeys().at(0)).toString().split(",", QString::SkipEmptyParts));
    while(true)
    {
        closeBlacklistedProcesses();
    }
}

void DefenseThread::closeBlacklistedProcesses(void)
{
//    qDebug() << "Closing blacklisted processes...";
    HANDLE hProcess;
    DWORD dwExitCode = 0;
    HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    if (hSnapshot)
    {
//        qDebug() << "Successfully got process snapshot";
        PROCESSENTRY32 pe32;
        pe32.dwSize = sizeof(PROCESSENTRY32);
        if(Process32First(hSnapshot, &pe32))
        {
//            qDebug() << "Successfully got process 1";
            do
            {
                QString process = QString::fromWCharArray(pe32.szExeFile);
//                qDebug() << "Process name is" << process;
                for(int i = 0; i < list->size(); i++)
                {
                    QString blacklisted = list->at(i);
//                    qDebug() << "Checking against blacklisted process:" << blacklisted;
                    if (process.toLower() == blacklisted.toLower())
                    {
//                        qDebug() << blacklisted << "spotted...terminating...";
                        if(hProcess = OpenProcess(PROCESS_ALL_ACCESS, 0, pe32.th32ProcessID))
                        {
                            GetExitCodeProcess(hProcess, &dwExitCode);
                            TerminateProcess(hProcess, dwExitCode);
                            CloseHandle(hProcess);
                        }
                    }
                }
            } while(Process32Next(hSnapshot, &pe32));
            CloseHandle(hSnapshot);
        }
    }
}
