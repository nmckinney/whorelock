#ifndef REDDITTEST_H
#define REDDITTEST_H

#include <QObject>

class RedditTest : public QObject
{
    Q_OBJECT
public:
    explicit RedditTest(QObject *parent = 0);
    void test(void);
signals:
    
public slots:
    
};

#endif // REDDITTEST_H
