#ifndef INSTALL_H
#define INSTALL_H

#include <QObject>
#include <QFile>
#include <QDir>
#include <QString>
#include <QSettings>
#include <QCoreApplication>

class Install : public QObject
{
    Q_OBJECT
private:
    bool copyFilesToDir(QString& dir);
public:
    explicit Install(QObject *parent = 0);
    static QString getInstallPath(void);
    bool install(void);
    void registry(void);
};

#endif // INSTALL_H
