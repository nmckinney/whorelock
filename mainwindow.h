#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWebView>
#include <QDebug>
#include <QString>
#include <QFile>
#include "install.h"
#include <QInputDialog>
#include <QLineEdit>
#include "reddit.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private:
    Ui::MainWindow *ui;
    Reddit* redd;
public slots:
    void getCode(void);
    void getTextValue(QString str);
};

#endif // MAINWINDOW_H
