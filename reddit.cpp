#include "reddit.h"

#define REDDIT_LOGIN "http://www.reddit.com/api/login"
#define REDDIT_COMMENT "http://www.reddit.com/api/comment"

Reddit::Reddit(QObject *parent, QString code) :
    QObject(parent)
{
    this->code = code;

}

void Reddit::start(void)
{
//    qDebug() << "starting reddit process...";
    qnam = new QNetworkAccessManager(this);
    QObject::connect(qnam, SIGNAL(finished(QNetworkReply*)), this, SLOT(gotReply(QNetworkReply*)));
    getCredentialsFromConfig();

    QObject::connect(this, SIGNAL(loggedIn()), this, SLOT(findSubreddit()));
    QObject::connect(this, SIGNAL(gotSubreddit()), this, SLOT(sendCode()));
    comment();
}

void Reddit::getCredentialsFromConfig(void)
{
    QSettings settings(":/files/config.ini", QSettings::IniFormat);
    settings.beginGroup("reddit");
    loginUser.first = settings.value("login_user").toString();
    loginUser.second = settings.value("login_password").toString();
    subreddit = settings.value("subreddit").toString();
}

void Reddit::login(void)
{
//    qDebug() << "in login()...";
    QUrl login_path(REDDIT_LOGIN);

    QHttpPart user;
    user.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"user\""));
    user.setBody(loginUser.first.toUtf8());

    QHttpPart passwd;
    passwd.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"passwd\""));
    passwd.setBody(loginUser.second.toUtf8());

    QHttpPart api_type;
    api_type.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"api_type\""));
    api_type.setBody("json");

    QList<QHttpPart>* list = new QList<QHttpPart>;
    list->append(user);
    list->append(passwd);
    list->append(api_type);

    sendRequest(login_path, POST, list);
//    qDebug() << "Sent request";
}

void Reddit::findSubreddit(void)
{
    QUrl subreddit_path("http://www.reddit.com/r/" + subreddit + "/.json");
    sendRequest(subreddit_path, GET);
}

void Reddit::sendCode(void)
{
    QUrl comment_path(REDDIT_COMMENT);

    QHttpPart text;
    text.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"text\""));
    text.setBody(code.toUtf8());

    QHttpPart id;
    id.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"thing_id\""));
    id.setBody(thingID.toUtf8());

    QHttpPart uh;
    uh.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"uh\""));
    uh.setBody(modhash.toUtf8());

    QList<QHttpPart>* list = new QList<QHttpPart>;
    list->append(text);
    list->append(id);
    list->append(uh);

    sendRequest(comment_path, POST, list);
}

void Reddit::comment(void)
{
//    qDebug() << "in comment()...";
    login();
}

void Reddit::sendRequest(QUrl url, HTTP_REQUEST_TYPE type_request, QList<QHttpPart>* list)
{
//    qDebug() << "in sendRequest()...";
    QHttpMultiPart* multipart;
    if (list != NULL)
    {
//        qDebug() << "Form list not null";
        multipart = new QHttpMultiPart(QHttpMultiPart::FormDataType);
        for(int i = 0; i < list->size(); i++)
        {
            multipart->append(list->at(i));
        }
    }
    else
    {
        type_request = GET;
    }
    QNetworkRequest req(url);
    if (type_request == GET) qnam->get(req);
    if (type_request == POST) qnam->post(req, multipart);
    delete list;
//    qDebug() << "finished sendRequest()";
}

void Reddit::gotReply(QNetworkReply* reply)
{
//    qDebug() << "Got network reply";
    if(reply->error() == QNetworkReply::NoError)
    {
        QJsonParseError error;
        QJsonDocument jsonDoc = QJsonDocument::fromJson(reply->readAll(), &error);
        if (error.error != QJsonParseError::NoError) return;
        QJsonObject jsonObj = jsonDoc.object();
        if(reply->url().toString() == REDDIT_LOGIN)
        {
//            qDebug() << "Logging in...";
            reply->close();
            delete reply;
            emit loggedIn();
        }
        else if(reply->url().toString() == REDDIT_COMMENT)
        {
//            qDebug() << "Commenting...";
            emit sentCode();
        }
        else if(reply->url().toString() == "http://www.reddit.com/r/" + subreddit + "/.json")
        {
            modhash =  jsonObj.value("data").toObject().value("modhash").toString();
            thingID = jsonObj.value("data").toObject().value("children").toArray().first().toObject().value("data").toObject().value("name").toString();
//            qDebug() << "Modhash =" << modhash;
//            qDebug() << "Thing ID =" << thingID;
            emit gotSubreddit();
        }
    }
    else
    {
        qDebug() << reply->errorString();
    }
}

