#ifndef ACTIVETHREAD_H
#define ACTIVETHREAD_H

#include <QObject>
#include <QGraphicsWidget>
#include <QApplication>
#include <QWidget>

class ActiveThread : public QObject
{
    Q_OBJECT
private:
    QWidget* widget;
public:
    explicit ActiveThread(QObject *parent = 0, QWidget* widget = NULL);
signals:
    
public slots:
    void threadStart(void);
};

#endif // ACTIVETHREAD_H
