#ifndef DEFENSETHREAD_H
#define DEFENSETHREAD_H

#include <QObject>
#include <Windows.h>
#include <TlHelp32.h>
#include <QString>
#include <QStringList>
#include <QSettings>
#include <QDebug>
#include <tchar.h>
#include <Psapi.h>

class DefenseThread : public QObject
{
    Q_OBJECT
private:
    QStringList* list;
    void closeBlacklistedProcesses(void);

public:
    explicit DefenseThread(QObject *parent = 0);
    
signals:

public slots:
    void threadStart(void);
};

#endif // DEFENSETHREAD_H
